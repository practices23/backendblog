/**
 * 
 * Encargado de llamar los métodos correspondientes para las APIs
 * 
 */

const { request } = require("express");
const { response } = require("express");

class PublicationController {
    constructor(publicationRepository){        
        this.publicationRepository=publicationRepository
    }


    /**
     * Crear nueva publicacion
     * 
     * @param {*} req 
     * @param {*} res 
     * 
     */
    async createNewPublication(req = request, res = response ) {
        try {
            const { code, message, data } = await global.publicationService.handleSavePublication(req.body);
            return res.status(code).json({ code, message, data });
        } 
        catch (error) {
            console.log(error);
            return res.status(501).json({ code: 501, message: "Error al crear publicacion"});
        }
    }


    /**
     * Función para obtener las publicaciones
     * 
     * @return {Json} 
     */
    async getPublication(req = request, res = response){

        try {
            
            const { code, message, data } = await global.publicationService.handleGetPublication();

            return res.status(code).json({ code, message, data });
            
        } catch (error) {
            
            console.log(error);
            return res.status(500).json({ code: 500, message: "Error en servidor, favor de intentar mas tarde", data: [] });

        }
        
    }

    /**
     * Función para obtener una publicacion dado un ID
     * 
     * @param {*} req 
     * @param {*} res 
     * 
     * @return {Json} 
     */
    async getPublicationById (req=request, res=response) {        
        try {
            const {code, message, data} = await global.publicationService.handleGetPublicationByID(req.params)
            return res.status(200).json({ code, message, data });
        }
        catch (e) {
            return res.status(501).json({ code: 501, message:"Error al obtener tickets" });
        }
    }

}

module.exports = PublicationController;

