/**
 * 
 * Apartado donde se establecen las rutas de los servicios
 * 
 */
const { Router } = require("express");
const { check }  = require('express-validator');

const PublicationController = require('./PublicationController');
const PublicationRepository = require('./PublicationRepository');
const PublicationService    = require('./PublicationService');

global.publicationRepository = new PublicationRepository();
global.publicationService    = new PublicationService(global.publicationRepository);

const router = Router();

/* Llamado a controladores */
const publicationController  = new PublicationController;

//Generar publicacion
router.post('/create',[
    check('title', 'El title no es valido').not().isEmpty(),
    check('author', 'El title no es valido').not().isEmpty(),
    check('content', 'El title no es valido').not().isEmpty(),
    ], 
    publicationController.createNewPublication);

//obtener publicaciones
router.get('/', publicationController.getPublication);

//obtener publicaciones por id
router.get('/:_id', publicationController.getPublicationById);

module.exports = router;
