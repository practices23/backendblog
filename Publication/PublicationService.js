/**
 * En este apartado se harán todas las funciones de lógica del negocio
 * 
 */



class PublicationService {

    constructor(publicationRepository) {
        this.publicationRepository = publicationRepository;
    }

    /**
     * Encargado de guardar información del publicacion
     * 
     * @param {Json} payload Información del publicacion
     * 
     * @returns Json
     */
    async handleSavePublication( payload ) {

        const { code, message,data } = await this.publicationRepository.savePublication(payload);
        return { code, message, data };

    }

    /**
     * Encargado de obtener las publicaciones
     * 
     * 
     * @returns Json
     */
    async handleGetPublication() {

        try {
            
            const { code, message,data } = await this.publicationRepository.getPublication();

            return { code, message, data };

        } catch (error) {
            console.log(error);
            return { code: 500, message: "Hubo un error interno, intente mas tarde", data: error };
        }

    }

        /**
     * Encargado de obtener las publicaciones por is
     * 
     * 
     * @returns Json
     */
    async handleGetPublicationByID(payload) {

        try {
            
            const { code, message,data } = await this.publicationRepository.getPublicationById(payload);

            return { code, message, data };

        } catch (error) {
            console.log(error);
            return { code: 500, message: "Hubo un error interno, intente mas tarde", data: error };
        }

    }
        getPublications

}

module.exports = PublicationService;
