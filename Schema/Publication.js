const { Schema, model } = require('mongoose');


const PublicationSchema = Schema({

    title: {
        type: String,
        required: true,
    },       
    author: {
        type: String,
        required:true,
    },         
    createdAt: {
        type: Date, 
        default: Date.now,
        required:true,
    },          
    content: {
        type: String,
        required: false,
    },
})

module.exports = model('Publication', PublicationSchema)